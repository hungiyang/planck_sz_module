from cosmosis.datablock import names, option_section
from cosmosis.datablock.cosmosis_py.section_names import likelihoods, mass_function
import numpy as np


cosmopara = names.cosmological_parameters

def setup(options):
    return 0  


def execute(block, config):
    Om = block[cosmopara, 'omega_m']
    h = block[cosmopara, 'h0']
    sigma_8 = block[cosmopara, 'sigma_8']
    Ob = block[cosmopara, 'omega_b']
    ns = block[cosmopara, 'n_s']
    
    like = -((Om-0.3)/0.02)**2 -  ((sigma_8 - 0.75)/0.01)**2 - ((sigma_8 - 0.75)+(h-0.673))**2 #- ((Ob-0.022)/0.01)**2 - ((t-0.08 - h/10.)/0.01)**2
    block[likelihoods, 'PLANCK_SZ_LIKE'] = like
    block[cosmopara,'new'] = Om**2 + h**0.3 + sigma_8
    return 0

    


def cleanup(config):
    return 0

