from astropy.io import fits
import numpy as np
from bisect import bisect_left
import matplotlib.pyplot as plt

#import union catalogue
filename = '/asiaa/home/summerstudents/ssp201501/Planck_data/HFI_PCCS_SZ-union_R2.08.fits'
p_union = fits.open(filename)
p_x = p_union[1].data
#p_x = p_x[np.squeeze(np.argwhere(p_x["MSZ"]!=0))]

#import MMF3 catalogue
filename = '/asiaa/home/summerstudents/ssp201501/Planck_data/HFI_PCCS_SZ-MMF3_R2.08.fits'
raw_MMF3 = fits.open(filename, memmape = True)
p_MMF3 = raw_MMF3[1].data


#select the clusters in the cosmology sample ('COSMO' = True)
#acquire the redshift from the union catalogue
if_COSMO = np.where([p_x['COSMO'][i-1] for i in p_MMF3['INDEX']])[0]
redshift_MMF3 = np.array([p_x['REDSHIFT'][i-1]   for i in p_MMF3['INDEX']])[if_COSMO]
SNR_MMF3 = p_MMF3['SNR'][if_COSMO]


#select the clusters detected with MMF3
#'PIPE_DET' has the form XXX, while X can be 0 or 1, and the tens digit is the MMF3
#if_MMF3 = np.where(np.array([str(i+1000)[2] == '1' for i in p_x['PIPE_DET']]))[0]
if_intersect = np.where(p_x['PIPE_DET'] == 111)[0]
if_COSMO = np.where(p_x['COSMO'])[0]
#MMF3_index = np.intersect1d(if_MMF3,if_COSMO)
intersect_index = np.intersect1d(if_intersect,if_COSMO)

redshift_intersect = p_x['REDSHIFT'][intersect_index]
SNR_intersect = p_x['SNR'][intersect_index]

#Choose MMF3 or intersect catalogue
redshift = redshift_MMF3
SNR = SNR_MMF3

#create a bar chart for number counts in different redshift bins, with specified SNR cutoff
def count_per_redshift():
    binsize = 0.1
    binnumber = 12
    bin_left = np.array([binsize*i for i in range(binnumber)])
    bin_central = np.array([binsize*(i+0.5) for i in range(binnumber)])

    def create_bar(q = 6):
        count  = np.zeros(binnumber)
        for i, r in enumerate(redshift):
            if SNR[i] > q:
                bin_i = bisect_left(bin_left,r)
                count[bin_i-1] += 1
        return count


    [q6, q7, q8_5] = [create_bar(q) for q in np.array([6,7,8.5])]
    e6 = np.array([np.sqrt(i) for i in q6])
    e7 = np.array([np.sqrt(i) for i in q7])
    e8_5 = np.array([np.sqrt(i) for i in q8_5])

    line6, = plt.plot(bin_central,q6,'ro')
    error6 = plt.errorbar(bin_central,q6,yerr = e6,linestyle="None",color='red')
    line7, = plt.plot(bin_central,q7,'bo')
    error7 = plt.errorbar(bin_central,q7,yerr = e7,linestyle="None",color='b')
    line8_5, = plt.plot(bin_central,q8_5,'go')
    error8_5 = plt.errorbar(bin_central,q8_5,yerr = e8_5,linestyle="None",color='g')
    
    plt.legend([line6, line7, line8_5],[r'$q_{cut} = 6$',r'$q_{cut} = 7$',r'$q_{cut} = 8.5$'],fontsize = 20, numpoints = 1)
    plt.xlabel('redshift',fontsize = 20)
    plt.ylabel('cluster counts',fontsize = 20)
    plt.tick_params(axis = 'y', labelsize = 15)
    plt.tick_params(axis = 'x', labelsize = 15)
    plt.ylim([0,140])
    plt.xlim([0,1])
    plt.show()
    return None

#calculate the number counts of clusters per redshift per SNR
z_left = np.linspace(0, 0.9, 10)
z = z_left + 0.05
q_left = np.linspace(6., 10., 5)
q_right = q_left + 1.
q_right[-1] = 100
q_cen = q_left + 0.5

#first index labels different redshift, second index labels different log(q)
count = np.zeros((len(z),len(q_cen)), dtype = 'int')
for r, q in np.transpose([redshift, SNR]):
    if r >= z_left[-1] + 0.05 or r < 0 or q >= q_right[-1] or q < q_left[0] :
        pass
    else:
        count[bisect_left(z_left,r)-1,bisect_left(q_left,q)-1] += 1
        #if bisect_left(z_left,r) == 0:
            #print r,q




#logq_left = np.array([np.log(5.5) + 0.25*i for i in range(4)])
#logq = logq_left + 0.125

##first index labels different redshift, second index labels different log(q)
#count = np.zeros((len(z),len(logq)))
#for r,lq in np.transpose([redshift,np.log(SNR)]):
    #if r >= z_left[-1] + 0.05 or lq >= logq_left[-1] + 0.125:
        #pass
    #else:
        #count[bisect_left(z_left,r)-1,bisect_left(logq_left,lq)-1] += 1





##plot the number of clusters at q>10
#z_left = np.arange(0,1,0.1)
#z = z_left + 0.05
#highqcount = np.zeros((len(z)), dtype = 'int')
#for r, q in np.transpose([redshift, SNR]):
    #if r >= z_left[-1] + 0.05 or q <= 10:
        #pass
    #else:
        #highqcount[bisect_left(z_left,r)-1] += 1

#plt.plot(z,highqcount,'o')
#plt.xlabel('redshift')
#plt.ylabel('number of q>10 clusters')
#plt.title('number of clusters observed at q>10')
#plt.show()




