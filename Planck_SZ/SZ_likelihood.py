import numpy as np
import scipy.misc as m

#return log of poisson
def poisson(k, l):
    #return l**k * np.e**(-l)/m.factorial(k)
    return k*np.log(l) - l - np.log(m.factorial(k))
    
def likelihood(exp, pre): #a, b are 2d numpy array with the same dimension
    if exp.shape != pre.shape:
        print 'input array doesn\'t have the same dimension!'
        return 0.
    return sum(np.array([poisson(pair[0],pair[1]) for pair in np.transpose([exp.flatten(),pre.flatten()])]))

#dN/dzdq
def SZ_likelihood(predict):
    exp = np.array([[30, 19,  7,  8, 42],
       [27, 17, 18, 11, 29],
       [26, 22, 18,  8, 29],
       [29, 12, 12,  7,  9],
       [ 8,  6,  4,  2, 10],
       [ 3,  5,  5,  0,  2],
       [ 3,  1,  1,  0,  0],
       [ 0,  0,  0,  0,  0],
       [ 0,  0,  0,  0,  2],
       [ 0,  0,  0,  0,  0]])
    if exp.shape != predict.shape:
        print 'input array doesn\'t have the same dimension!'
        raise Exception('input array doesn\'t have the same dimension!')
        return 0.

    return likelihood(exp, predict)

#to simulate the result of 2013, dN/dz
def SZ_likelihood_only_z(predict, z = 6):
    cut_index = round(z - 6)
    raw = np.array([[30, 19,  7,  8, 42],
       [27, 17, 18, 11, 29],
       [26, 22, 18,  8, 29],
       [29, 12, 12,  7,  9],
       [ 8,  6,  4,  2, 10],
       [ 3,  5,  5,  0,  2],
       [ 3,  1,  1,  0,  0],
       [ 0,  0,  0,  0,  0],
       [ 0,  0,  0,  0,  2],
       [ 0,  0,  0,  0,  0]])
    exp = np.array([sum(i) for i in raw[:,cut_index:]])
    pre = np.array([sum(i) for i in predict])
    if exp.shape != pre.shape:
        print 'input array doesn\'t have the same dimension!'
        raise Exception('input array doesn\'t have the same dimension!')
        return 0.
        
    return likelihood(exp, pre)
    
    
if __name__ == '__main__':
    a = np.ones([10,5])
    print SZ_likelihood(a)

