from cosmosis.datablock import names, option_section
from cosmosis.datablock.cosmosis_py.section_names import likelihoods, mass_function
import numpy as np

import SZ_likelihood as lk 
import massfunction_to_dNdzdq as m2c

cosmopara = names.cosmological_parameters
#I 've added a new section name scaling_relation_parameters, so the line below works. To let others run the code, I'll just type the string manually instead
#sr = names.scaling_relation_parameters
sr = 'scaling_relation_parameters'

def setup(options):
    only_z = options.get_bool(option_section,'only_z',default = False)
    only_z_cutoff = options.get_int(option_section,'only_z_cutoff',default = 8)
    verbal = options.get_bool(option_section,'verbal',default = False)
    save_nan = options.get_string(option_section, 'save_nan', default = 'nan_case.txt')
    return (only_z, only_z_cutoff,verbal, save_nan)


def execute(block, config):
    #get the values from setup
    only_z, only_z_cutoff, verbal, save_nan = config
    
    #get the values from the values.ini
    Om = block[cosmopara, 'omega_m']
    h = block[cosmopara, 'h0']
    alpha = block[sr, 'alpha']
    b = block[sr, 'b']
    c = block.get_double(sr, 'c',4.0)
    sigma_lnY = block.get_double(sr, 'sigma_lnY', 0.127)

    cosmo = m2c.Cosmology(Om = Om, h = h, b = b, alpha = alpha, c = c, sigma_lnY = sigma_lnY)
    #use the mass function output from the previous module <tinker_MF> in the pipeline
    cosmo.mf = block[mass_function, 'dndlnmh']
    cosmo.r = block[mass_function, 'r_h']
    cosmo.z = block[mass_function, 'z']
    
    if only_z:
        cosmo.create_cluster_count_qbin(qrange = [only_z_cutoff, 10.,2])
        like = lk.SZ_likelihood_only_z(cosmo.cluster_count, z = only_z_cutoff)

    else:
        cosmo.create_cluster_count_qbin()
        like = lk.SZ_likelihood(cosmo.cluster_count)
    
    #check if result is nan, save the input paramter if it is, and change the likelihood to -2000
    if np.isnan(like):
        sigma8 = block[cosmopara, 'sigma8_input']
        omega_b = block[cosmopara, 'omega_b']
        n_s = block[cosmopara, 'n_s']
        params = np.array([[sigma8, Om, h, omega_b, n_s,  alpha, b]])
        like = -5000.
        with open(save_nan, 'a') as nanfile:
            np.savetxt(nanfile, params)
    
    
    if verbal:
        print 'Planck likelihood: ' + str(like)
    block[likelihoods, 'PLANCK_SZ_LIKE'] = like
    return 0

    


def cleanup(config):
    return 0
