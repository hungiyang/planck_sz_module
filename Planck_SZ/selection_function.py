from astropy.io import fits
import numpy as np
from bisect import bisect_left
import scipy.interpolate as sint


class selection_function:
    def __init__(self, sigma_lnY = 0.127, path = '/asiaa/home/summerstudents/ssp201501/Planck_data/HFI_PCCS_SZ-selfunc-MMF3-cosmolog_R2.08.fits'):
        fitsdata = fits.open(path)
        selfunc = fitsdata[2].data
        ygrid = fitsdata[3].data
        tgrid = fitsdata[4].data
        qgrid = fitsdata[5].data
        fitsdata.close()
        self.ygrid = ygrid
        self.tgrid = tgrid
        self.qgrid = qgrid
        self.selfunc = selfunc
        self.sigma_lnY = sigma_lnY
    
    #P's are not used in the analysis
    #This one is with spline interpolation, and the nearest y and theta
    def P(self, y = 0.002, t = 10, q = 7.5):
        #look for the closest Y and t
        if not (self.ygrid[0] < y < self.ygrid[-1] and self.tgrid[0] < t < self.tgrid[-1]):
            return 0.
        yi = np.abs(self.ygrid-y).argmin()
        ti = np.abs(self.tgrid-t).argmin()
        spl = sint.splrep(self.qgrid, self.selfunc[:,ti,yi],s = 0, k = 2) 
        prob = -sint.splev(q,spl,der = 1)
        if prob < 0:
            return 0.
        return prob

    #This one doesn't have spline interpolation
    def P_simple(self, y = 0.002, t = 10, q = 7.5):
        if not (self.ygrid[0] < y < self.ygrid[-1] and self.tgrid[0] < t < self.tgrid[-1] and self.qgrid[0] < q < self.qgrid[-1]):
            return 0.
        yi = np.abs(self.ygrid-y).argmin()
        ti = np.abs(self.tgrid-t).argmin()
        qi = bisect_left(self.qgrid, q)-1
        return (self.selfunc[qi, ti, yi] - self.selfunc[qi + 1, ti, yi])/0.5
    
    ##this function is incorrect, RIP
    def P_scatter(self, y = 0.002, t = 10., q = 7.5, dq = 0.1):
        return -(self.completeness_scatter(y, t, q + dq) - self.completeness_scatter(y, t, q))/dq


    #consider the intrinsic scatter (see the note)
    #integrate over nearby y
    def scatter(self, qm, qmbar):
        sigma = self.sigma_lnY
        return 1./(np.sqrt(2*np.pi)*sigma)*np.e**(-np.log(qm/float(qmbar))**2/(2.*sigma**2))

    def completeness_scatter(self, y = 0.002, t = 10, q = 7.5, n = 7):
        if not (self.ygrid[0] < y < self.ygrid[-1] and self.tgrid[0] < t < self.tgrid[-1]):
            return 0.
        
        dlnY = np.log(1.24960923)       #every neighboring Y inn Y grid has this ratio difference
        yi = np.abs(self.ygrid - y).argmin()
        #set it so that it doesn't integrate outside the ygrid range
        if yi < n:
            yi = n
        elif yi + n >= len(self.ygrid):
            yi = len(self.ygrid) -1 -n
        ti = np.abs(self.tgrid - t).argmin()
        qi = bisect_left(self.qgrid, q)
        dqright = (q - self.qgrid[qi-1])/0.5
        dqleft = (self.qgrid[qi] - q)/0.5
        dtright = (t - self.tgrid[ti-1])/(self.tgrid[ti] - self.tgrid[ti-1])
        dtleft = (self.tgrid[ti] - t)/(self.tgrid[ti] - self.tgrid[ti-1])
        
        #interpolate in the nearest 4 points in the (theta, q) plane
        comp_left_left = sum(np.array([self.selfunc[qi-1, ti-1, yi + i]*self.scatter(self.ygrid[yi + i], y) for i in range(-n,n+1)])) * dlnY
        comp_right_left = sum(np.array([self.selfunc[qi, ti-1, yi + i]*self.scatter(self.ygrid[yi + i], y) for i in range(-n,n+1)])) * dlnY
        comp_left_right = sum(np.array([self.selfunc[qi-1, ti, yi + i]*self.scatter(self.ygrid[yi + i], y) for i in range(-n,n+1)])) * dlnY
        comp_right_right = sum(np.array([self.selfunc[qi, ti, yi + i]*self.scatter(self.ygrid[yi + i], y) for i in range(-n,n+1)])) * dlnY
        #make sure the integral doens't exceed 1.0
        com = min(1.0, dqleft*dtleft*comp_left_left + dqright*dtleft*comp_right_left + dqleft*dtright*comp_left_right + dqright*dtright*comp_right_right)
        return com
        
        
    #this only does interpolation in q, and find the closest theta and Y
    def completeness_old(self, y = 0.002, t = 10., q = 7.5):
        if not (self.ygrid[0] < y < self.ygrid[-1] and self.tgrid[0] < t < self.tgrid[-1]):
            return 0.
        yi = np.abs(self.ygrid-y).argmin()
        ti = np.abs(self.tgrid-t).argmin()
        spl = sint.splrep(self.qgrid, self.selfunc[:,ti,yi],s = 0, k = 2) 
        return sint.splev(q,spl)        
        
    # interpolate with the nearest 8 points
    def completeness(self, y = 0.002, t = 10, q = 7.5):
        if not (self.ygrid[0] < y < self.ygrid[-1] and self.tgrid[0] < t < self.tgrid[-1]):
            return 0.
        yi = bisect_left(self.ygrid, y)
        ti = bisect_left(self.tgrid, t)
        qi = bisect_left(self.qgrid, q)
        
        ydis = np.array([self.ygrid[yi] - y, y - self.ygrid[yi-1]])/(self.ygrid[yi] - self.ygrid[yi-1])
        tdis = np.array([self.tgrid[ti] - t, t - self.tgrid[ti-1]])/(self.tgrid[ti] - self.tgrid[ti-1])
        qdis = np.array([self.qgrid[qi] - q, q - self.qgrid[qi-1]])/(self.qgrid[qi] - self.qgrid[qi-1])
        
        indexlist = np.array([[[[i,j,k]for k in range(2)]for j in range(2)]for i in range(2)]).reshape(8,3)
        
        com = sum([self.selfunc[qi-1 + i[0], ti-1+i[1],yi-1+i[2]]*qdis[i[0]]*tdis[i[1]]*ydis[i[2]] for i in indexlist])
        return com
        
        
        
        

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})
    #old code for testing
    #import selection function 
    filename = '/asiaa/home/summerstudents/ssp201501/Planck_data/HFI_PCCS_SZ-selfunc-MMF3-cosmolog_R2.08.fits'
    fitsdata = fits.open(filename)
    mask = fitsdata[1].data
    selfunc = fitsdata[2].data
    ygrid = fitsdata[3].data
    tgrid = fitsdata[4].data
    qgrid = fitsdata[5].data
    fitsdata.close()
        
        
    ##plot gaussian of intrinsic scatter
    #def scatter(qm, qmbar):
        #sigma = 0.127
        #return 1./(np.sqrt(2*np.pi)*sigma)*np.e**(-np.log(qm/float(qmbar))**2/(2.*sigma**2))
    #Ym = 0.001
    #Y = np.arange(0.0005,0.002,0.00001)
    #P = np.array([scatter(y,Ym) for y in Y])
    #plt.figure(figsize = (8,6))
    #plt.plot(np.log(Y),P,linewidth = 2)
    #plt.xlabel(r'$\ln Y(\mathrm{arcmin})$', fontsize = 25)
    #plt.ylabel(r'$P[\ln{Y}|\bar{Y}]$',fontsize = 25)
    #plt.tick_params(axis = 'x', labelsize = 20)
    #plt.tick_params(axis = 'y', labelsize = 20)
    #plt.show()
    
    
    
    #plot the difference between completeness and completeness_scatter
    sel = selection_function(sigma_lnY = 0.127)
    y = 0.0015
    t = 8.0
    q = 6
    
    qgrid = np.arange(4.6,10,0.1)
    com = np.array([sel.completeness(y,t,q) for q in qgrid])
    com_scatter = np.array([sel.completeness_scatter(y,t,q, n = 5) for q in qgrid])

    plt.plot(qgrid, com, qgrid, com_scatter)
    plt.legend([r'$\chi(Y_{500},\theta_{500},q)$',r'$\hat{\chi}(\bar{Y}_{500},\bar{\theta}_{500},q)$'],fontsize = 20, loc = 'upper right')
    plt.xlabel('q', fontsize = 20)
    plt.ylabel(r'$\chi / \hat{\chi}$',fontsize = 20)
    plt.title(r'$Y = \bar{Y}$ = ' + str(y) + r', $\theta = \bar{\theta} = $' + str(t),fontsize = 20)
    plt.tick_params(axis = 'x', labelsize = 20)
    plt.tick_params(axis = 'y', labelsize = 20)
    plt.ylim([0,1])
    plt.xlim([4.5,10])
    plt.show()
    
    
    ##plot completeness and completenss_scatter against y
    #sel = selection_function(sigma_lnY = 0.5)
    #t = 15.0
    #q = 6
    
    #ygrid = np.linspace(0.0004,0.010,100)
    #com = np.array([sel.completeness(y,t, q) for y in ygrid])
    #com_scatter = np.array([sel.completeness_scatter(y,t, q, n = 7) for y in ygrid])
    
    #plt.plot(1000*ygrid, com, 1000*ygrid, com_scatter)
    #plt.legend([r'$\chi(Y_{500},\theta_{500},q)$',r'$\hat{\chi}(\bar{Y}_{500},\bar{\theta}_{500},q)$'],fontsize = 20, loc = 'lower right')
    #plt.xlabel(r'$Y/\bar{Y}  (10^{-3} \mathrm{arcmin}^2)$', fontsize = 20)
    #plt.ylabel(r'$\chi / \hat{\chi}$',fontsize = 20)
    #plt.title(r'$q  = $' + str(q) + r', $\theta = \bar{\theta} = $' + str(t) + ' (arcmin)',fontsize = 20)
    #plt.tick_params(axis = 'x', labelsize = 20)
    #plt.tick_params(axis = 'y', labelsize = 20)
    #plt.ylim([0,1.05])
    ##plt.xlim([4.5,10])
    #plt.show()
    
    
    
    
    
    
    ## plot the completeness and the selection function P 
    #y = 0.002
    #t = 10.0
    #q = 6

    #yi = np.abs(ygrid-y).argmin()
    #ti = np.abs(tgrid-t).argmin()

    #grid = np.arange(4.5,10,0.1)
    #spline = sint.splrep(qgrid, selfunc[:,ti,yi],s = 0,k = 2)
    #p = sint.splev(grid,spline)
    #pd = -sint.splev(grid,spline, der = 1)

    #plt.plot(grid, p, grid, pd)
    #plt.plot(qgrid, selfunc[:,ti,yi],'ro')
    #plt.legend(['completeness spline fit',r'P(q|qm(Y,$\theta$))','completeness data'])
    #plt.xlabel('q')
    #plt.ylabel('percentage')
    #plt.title('Y = ' + str(y) + r', $\theta = $' + str(t))
    #plt.show()
    
    
    ###plot the difference between P and P_scatter
    #y = 0.004
    #t = 10
    #sel = selection_function()
    #qlist = np.arange(4.6,10,0.1)
    #plist = np.array([sel.P(y = y, t = t,q = qq) for qq in qlist])
    #pscatterlist = np.array([sel.P_scatter(y = y, t = t,q = qq) for qq in qlist])
    #plt.plot(qlist, plist, qlist, pscatterlist)
    #plt.legend(['without scatter','integrate over scatter'],loc = 'upper left')
    #plt.xlabel('SNR')
    #plt.ylabel('P(q|qm)')
    #plt.title('Y = '+ str(y)  + r'$(arcmin^2), \theta = $' + str(t) + '(arcmin)')
    #plt.show()
    
    
    ##plot the difference between spline and not spline
    #y = 0.002
    #t = 9
    #sel = selection_function()
    #qlist = np.arange(4.5,10,0.01)
    #plist = np.array([sel.P(y = y, t = t,q = qq) for qq in qlist])
    #psimplelist = np.array([sel.P_simple(y = y, t = t,q = qq) for qq in qlist])
    #plt.plot(qlist, plist, qlist, psimplelist)
    #plt.legend(['spline','no spline'],loc = 'upper left')
    #plt.xlabel('SNR')
    #plt.ylabel('P(q|qm)')
    #plt.title('Y = '+ str(y)  + r'$(arcmin^2), \theta = $' + str(t) + '(arcmin)')
    #plt.show()
    
    
    

            
    #bad = 0
    #for i, arr in enumerate(mask):
            #for j,ele in enumerate(arr[0]):
                    #if ele < 0.1:
                            #bad += 1
                            
    #bad = 17616077
    
    #sel = selection_function()
    #print sel.P_scatter(), sel.P()



