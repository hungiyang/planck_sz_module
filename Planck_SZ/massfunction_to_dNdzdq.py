import numpy as np
import os
import bisect as bs
import cosmolopy.distance as cd
from scipy.interpolate import Rbf
from selection_function import selection_function


#define a cosmology class
class Cosmology:
    def __init__(self, Om = 0.275375, h = 0.673, b = 0.0, alpha = 1.79, c = 4. ,sigma_lnY = 0.127):    #Om: matter density, h: hubble constant, b: mass bias, 
        self.Om = Om
        self.Ol = 1 - Om
        self.h = h
        self.b = b
        self.aa = alpha
        self.c = float(c)
        self.sigma_lnY = sigma_lnY
        self.cosmo = {'omega_M_0' : Om, 'omega_lambda_0' : 1-Om, 'h' : h}
        cd.set_omega_k_0(self.cosmo)
        
    #read dndlnmh.txt from the tinker_MF output
    def read_mf(self, path):
        if not os.path.isfile(path):
            raise Exception('Error: file %s does not exist!'%path)
        with open(path) as f:
            raw = [line for line in f]
            data = np.array([[float(x) for x in line.split()] for line in raw[1:]])
        self.mf = data
    
    #read m_h.txt of the tinker_MF output
    def read_m(self, path):
        if not os.path.isfile(path):
            raise Exception('Error: file %s does not exist!'%path)
        with open(path) as f:
            raw = [line for line in f]
            data = np.array([float(x) for x in raw[1:]])
        self.m = data
    
    
    #read r_h.txt of the tinker_MF output
    def read_r(self, path):
        if not os.path.isfile(path):
            raise Exception('Error: file %s does not exist!'%path)
        with open(path) as f:
            raw = [line for line in f]
            data = np.array([float(x) for x in raw[1:]])
        self.r = data
        
    #read z.txt of the tinker_MF output
    def read_z(self, path):
        if not os.path.isfile(path):
            raise Exception('Error: file %s does not exist!'%path)
        with open(path) as f:
            raw = [line for line in f]
            data = np.array([float(x) for x in raw[1:]])
        self.z = data

    def r2Mt(self,r ,z = 0):   #r in units of Mpc, M in units of solar mass
        rho_m = 2.775366e11*self.Om*self.h**2 #in units of Ms Mpc^-3,  Om = 0.27, h = 0.71
        delta = 1.  #or 200?
        #M = 4*np.pi*r**3/3.0*delta*rho_m*(1.+z)**3
        M = 4*np.pi*r**3/3.0*delta*rho_m
        return M
        
    
    def Mt2r(self, M, z = 0):
        rho_m = 2.775366e11*self.Om*self.h**2 #in units of Ms Mpc^-3
        delta = 1.  #or 200?
        #r_h = (3*M/(4*np.pi*delta*rho_m*(1.0+z)**3))**(1.0/3.0)
        r_h = (3*M/(4*np.pi*delta*rho_m))**(1.0/3.0)
        return r_h

    #convert M from tinker mass function output to M_500 (Mv's functions are not used anymore, since tinker's output mass is not actually Mv)
    def x(self, f):
        (a1,a2,a3,a4) = (0.5116, -0.4283, -3.13e-3, -3.52e-5)
        p = a2 + a3*np.log(f) +a4*np.log(f)**2
        return (a1*f**(2*p)+0.75**2)**-0.5 + 2*f

    def f(self, x):
        return x**3.0*(np.log(1.0+1.0/x)-1.0/(1.0+x))
    
    def MttoM500(self, Mt, z = 0.):
        c = self.c
        #calculate the virial overdensity
        rho_m = self.Om*(1.+z)**3
        rho_c = self.Om*(1.+z)**3 + self.Ol
        Om_z = rho_m/rho_c
        x = Om_z - 1.
        delta_v = (18*np.pi**2 + 82*x - 39*x**2)/(1.+x)
        
        rt = 1/self.x(200.*rho_m/(delta_v*rho_m)*self.f(1./c))    #rt/r_v
        r500 = 1/self.x(500.*rho_c/(delta_v*rho_m)*self.f(1./c))
        #r500 = 2.6064408002636958     #r500/r_v, calculated with the equation above if c = 4
        ratio = (500.*rho_c)/(200.*rho_m)*(r500/rt)**3   #M500/Mt
        return Mt*ratio
        
    def M500toMt(self, M500, z = 0.):
        c = self.c
        #calculate the virial overdensity
        rho_m = self.Om*(1.+z)**3
        rho_c = self.Om*(1.+z)**3 + self.Ol
        Om_z = rho_m/rho_c
        x = Om_z - 1.
        delta_v = (18*np.pi**2 + 82*x - 39*x**2)/(1.+x)
                
        rt = 1/self.x(200.*rho_m/(delta_v*rho_m)*self.f(1./c))    #rt/r_v
        r500 = 1/self.x(500.*rho_c/(delta_v*rho_m)*self.f(1./c))
        #r500 = 2.6064408002636958     #r500/r_v, calculated with the equation above if c = 4
        ratio = (500.*rho_c)/(200.*rho_m)*(r500/rt)**3   #M500/Mt
        return M500/ratio

    #def cc(self, M200c, z):
        #A = 3.66   #\pm 0.16
        #B = -0.14  #\pm 0.52
        #C = -0.32 #\pm 0.18
        #return A*(1.37/(1.+z))**B*(M200c/(8.e14/self.h))**C
    
    ##add an extra correction of using the fitted formula in Merten & CLASH
    #def M500toMt(self, M500, z = 0.):    #this conversion uses the fitted formula from Merten & CLASH
        #c = self.c
        ##calculate the virial overdensity
        #rho_m = self.Om*(1.+z)**3
        #rho_c = self.Om*(1.+z)**3 + self.Ol
        #Om_z = rho_m/rho_c
        #x = Om_z - 1.
        #delta_v = (18*np.pi**2 + 82*x - 39*x**2)/(1.+x)
                
        ##this gets a correction for the concentration parameter, by assuming that 200c is close to the virial overdensity
        #r500_temp = 1/self.x(500.*rho_c/(delta_v*rho_m)*self.f(1./c))
        #ratio_500_200c = (500.*rho_c)/(delta_v*rho_m)*(r500_temp)**3
        #M200c = M500/ratio_500_200c
        #cc = self.cc(M200c,z)
        
        #rt = 1/self.x(200.*rho_m/(200*rho_c)*self.f(1./cc))    #rt/r_v
        #r500 = 1/self.x(500.*rho_c/(200*rho_c)*self.f(1./cc))
        #ratio = (500.*rho_c)/(200.*rho_m)*(r500/rt)**3   #M500/Mt
        #return M500/ratio
        
    
    #with the txt file from the <tinker_MF> module, interpolate the mass function given r_h and z with scipy Rbf function
    #this gives weird result, stupid...
    def tinker_MF_stupid(self, z, r_h):
        if not hasattr(self,'z') and hasattr(self,'r') and hasattr(self,'mf'):
            raise Exception('mass function data missing.') 
        
        r = r_h*self.h   #h=0.71
        z_i = bs.bisect_left(self.z, z)
        r_i = bs.bisect_left(self.r, r)
        n = 2
        zz = self.z[max(0, z_i-n):min(z_i + n+2, len(self.z))]
        rr = self.r[max(0, r_i-n):min(r_i + n+2, len(self.r))]
        
        zm = np.repeat(zz,len(rr))
        rm = np.tile(rr,len(zz))
        mf_inter = Rbf(zm, rm, self.mf[max(0, z_i-n):min(z_i + n+2, len(self.z)),max(0, r_i-n):min(r_i + n+2, len(self.r))].flatten())
        return mf_inter(z,r)*self.h**3    #see the output of tinker_MF module on the wiki page
        
    #simple interpolate between the nearest 4 points
    def tinker_MF(self, z, r_h):
        if not hasattr(self,'z') and hasattr(self,'r') and hasattr(self,'mf'):
            raise Exception('mass function data missing.') 
        
        r = r_h*self.h   #h=0.71
        zi = bs.bisect_left(self.z, z)
        ri = bs.bisect_left(self.r, r)
        zleft, zright = np.array([self.z[zi] - z, z - self.z[zi-1]])/(self.z[zi] - self.z[zi-1])
        rleft, rright = np.array([self.r[ri] - r, r - self.r[ri-1]])/(self.r[ri] - self.r[ri-1])
        return (self.mf[zi-1,ri-1]*zleft*rleft + self.mf[zi,ri-1]*zright*rleft + self.mf[zi-1,ri]*zleft*rright + self.mf[zi,ri]*zright*rright)*self.h**3
        
        
        
    
    #mass function plus the mass conversion
    def dNdMdz(self, M500, z):
        Mv = self.M500toMt(M500,z)
        r = self.Mt2r(Mv ,z)
        #calculate dMv/dM500
        delta = 100   #units of solar mass
        dMvdM500 = (self.M500toMt(M500+delta,z)-self.M500toMt(M500,z))/delta
        #dNdMdV
        dNdMdV = self.tinker_MF(z,r)/Mv*dMvdM500
        dV = cd.diff_comoving_volume(z, **self.cosmo) #/(1.0+z)**3    #if tinker_MF output is per comoving volume, then we don't need the (1+z)**3
        return dNdMdV*dV
    
    #scaling relation
    def Y500(self, M500, z):
        E = cd.e_z(z,**self.cosmo)
        DA = cd.angular_diameter_distance(z,**self.cosmo)
        aa = self.aa   #1.79
        bb = 0.66
        #b = 1. - 0.8    #1-b = 0.8
        Y = 10.**-0.19  
        
        Y500 = E**bb*1.e-4/DA**2.*Y*(self.h/0.7)**(-2.+aa)*((1.- self.b)*M500/6.e14)**aa
        return Y500/(np.pi/180./60.)**2  #convert from steradian to square arcmin
    
    def theta500(self, M500,z):
        E = cd.e_z(z,**self.cosmo)
        DA = cd.angular_diameter_distance(z,**self.cosmo)
        aa = self.aa   #1.79
        bb = 0.66
        #b = 1 - 0.8    #1-b = 0.8
        theta = 6.997  #arcmin
        
        theta500 = theta*(self.h/0.7)**(-2./3.)*((1 - self.b)*M500/3.e14)**(1./3.)*E**(-2./3.)/(DA/500.)
        return theta500

    #this function gives the dNdzdq at a particular z and q. For q bins and z bins, use the next function dNdzdq_Delta_q
    def dNdzdq(self, z ,q):
        mask = 0.65     #cosmological mask
        #intergrate from M500 = 1e13 to 1e15
        dm = 10.0e13
        mlist = np.arange(1.0e13,1.0e15,dm)
        sel = selection_function(sigma_lnY = self.sigma_lnY)     #selection_function object imported from selection_function.py
        dNdzdq = 4*np.pi*mask*sum([self.dNdMdz(m,z)*sel.P_scatter(self.Y500(m,z), self.theta500(m,z), q)*dm  for m in mlist])     #change whether to use P_scatter here
        return dNdzdq
        
    def dNdzdq_Delta_q(self, z, qmin, qmax):            #if qmax > 10, qmax = infinity
        mask = 0.65     #cosmological mask
        #intergrate from M500 = 1e13 to 1e15
        dm = 10.0e13
        mlist = np.arange(1.0e13,1.0e15,dm)
        sel = selection_function(sigma_lnY = self.sigma_lnY)     #selection_function object imported from selection_function.py
        if qmin > 10.:
            raise Exception('don\'t have selection function for q > 10')
        completeness = sel.completeness_scatter                     #change whether to consider intrinsic scatter or not
        # if qmax > 10, let qmax goes to infinity
        if qmax > 10.0:
            dNdzdq = 4*np.pi*mask*sum([self.dNdMdz(m,z)*completeness(self.Y500(m,z), self.theta500(m,z), 10.)*dm  for m in mlist])   #make the last bin goes up to q = infinity
        else:
            dNdzdq = 4*np.pi*mask*sum([self.dNdMdz(m,z)*(completeness(self.Y500(m,z), self.theta500(m,z), qmin)-completeness(self.Y500(m,z), self.theta500(m,z), qmax))*dm  for m in mlist])
        return dNdzdq
    
    #this doesn't consider the intrinsic scatter. It only calculates the difference of completeness of neighboring qbins. (see the function dNdzdq_Delta_q)
    def create_cluster_count_qbin(self, zrange = [0.00001, 0.9, 10],qrange = [6, 10, 5], nz = 3):
        zleft = np.linspace(*zrange)
        dz = zleft[1] - zleft[0]
        qleft = np.linspace(*qrange)
        dq = qleft[1] - qleft[0]
        count = np.zeros([len(zleft),len(qleft)])
        for i,z in enumerate(zleft):
            if nz == 1:
                zlist = np.array([z + dz/2.])
            else:
                zlist = np.linspace(z, z+dz, nz)
            for j,q in enumerate(qleft):
                count[i,j] = sum([self.dNdzdq_Delta_q(zz, q, q + dq) for zz in zlist])
        self.cluster_count = count*dz/nz  
        return 0.
    
    
    def plot_cluster_count(self, qcut = 0):
        if not hasattr(self,'cluster_count'):
            raise Exception('create_cluster_count() hasn\'t been run!') 
        #plot the cluster count above a cutoff q with respect to z (equivalent to Fig,4 in reading 1.1)
        qlist = np.linspace(6, 10, 5)
        count = np.array([[30, 19,  7,  8, 42],
                       [27, 17, 18, 11, 29],
                       [26, 22, 18,  8, 29],
                       [29, 12, 12,  7,  9],
                       [ 8,  6,  4,  2, 10],
                       [ 3,  5,  5,  0,  2],
                       [ 3,  1,  1,  0,  0],
                       [ 0,  0,  0,  0,  0],
                       [ 0,  0,  0,  0,  2],
                       [ 0,  0,  0,  0,  0]])

        zlist = np.linspace(0,0.9,10)+0.05
        pre_z = np.array([sum(row) for row in self.cluster_count[:,qcut:]])
        exp_z = np.array([sum(row) for row in count[:,qcut:]])
        
        flatzlist = np.repeat(zlist - 0.05,2)
        flatzlist = np.delete(flatzlist,1)
        flatzlist = np.append(flatzlist, zlist[-1] + 0.05)
        flatpre = np.repeat(pre_z,2)
        
        pre, = plt.plot(flatzlist, flatpre)
        
        plt.errorbar(zlist,exp_z,yerr = np.sqrt(exp_z),linestyle="None",color='red')
        exp, = plt.plot(zlist,exp_z, 'ro')
        plt.legend([pre,exp],[r'$N(z)$ best fit','observed counts'],loc = 'upper right',numpoints = 1)
        plt.xlim(0,1)
        plt.ylim(0,160)
        plt.xlabel(r'z', fontsize = 20)
        plt.ylabel(r'$N(z)$',fontsize = 20)
        plt.title(r'$q_{cut}$ = ' + str(qlist[qcut]), fontsize = 20)
        plt.tick_params(axis = 'x', labelsize = 15)
        plt.tick_params(axis = 'y', labelsize = 15)
        plt.show()
        
        return 0
        
        
        
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})

    test = Cosmology(Om = 0.348668 , b = 0.233808, h = 0.746591,  c = 4, alpha = 1.85046)
    test.read_mf('/asiaa/home/summerstudents/ssp201501/cosmosis_test/new_module_test/mass_function/dndlnmh.txt')
    test.read_z('/asiaa/home/summerstudents/ssp201501/cosmosis_test/new_module_test/mass_function/z.txt')
    test.read_r('/asiaa/home/summerstudents/ssp201501/cosmosis_test/new_module_test/mass_function/r_h.txt')
    test.read_m('/asiaa/home/summerstudents/ssp201501/cosmosis_test/new_module_test/mass_function/m_h.txt')
    
    ##check the conversion of r_h and m_h
    ###r = [test.r2Mt(test.r[i]*test.h**-1.)/(test.m[i]*test.Om*test.h**-1.) for i in range(50)]
    #test.create_cluster_count_qbin()
    #test.plot_cluster_count()

    ##investigate the concentration parameter c
    #zlist = np.linspace(0.0,1.0,11)
    #Om = 0.3*(1 + zlist)**2/(0.3*(1 + zlist) + 0.7)
    #x = Om - 1.
    #delta_v = (18*np.pi**2 + 82*x - 39*x**2)/(1.+x)
    #r = 1/np.array([test.x(200./(Om[i]*delta_v[i])*test.f(1/4.)) for i in range(len(zlist))])
    #rfit = 3.66*(1.37/(1+zlist))**-0.14
    #print r/rfit

    ##plot the mass function
    #def plot_massfunction(z = 0.0):
        #logmvlist = np.arange(10,16.,0.001)
        #mvlist = 10.**logmvlist
        #mf = np.array([test.tinker_MF(z,r)*test.r2Mt(r,z)**1/(2.775e11*0.3)  for r in test.Mt2r(mvlist,z)])     #dn/dm * 1/m^2
        #plt.plot(logmvlist, np.log10(mf))

        ###plt.title('z =' + str(z))
        ##plt.show()
    #plt.figure(figsize = (11,8))
    #for i in np.arange(0,1.1,0.5):
        #plot_massfunction(i)
    #plt.xlabel(r'$\log(M (Ms))$',fontsize = 25)
    #plt.ylabel(r'$\log (\frac{1}{M^2} \frac{dN}{dM})$',fontsize = 25)
    #plt.legend(['z = ' + str(z) for z in np.arange(0,1.1,0.5)],loc = 'lower left',fontsize = 25)
    #plt.tick_params(axis = 'x', labelsize = 20)
    #plt.tick_params(axis = 'y', labelsize = 20)
    #plt.ylim(-5,-1)
    
    ##plt.title('Tinker mass function')
    #plt.show()

    
    ##plot differential volume element
    #zlist = np.arange(0,4.,0.1)
    #VE = np.array([cd.diff_comoving_volume(z, **test.cosmo) for z in zlist])
    #plt.figure(figsize = (11,8))
    #plt.plot(zlist,VE)
    #plt.xlabel('redshift',fontsize = 25)
    #plt.ylabel(r'diff comoving volume $(Mpc^3)$', fontsize = 25)
    #plt.tick_params(axis = 'x', labelsize = 20)
    #plt.tick_params(axis = 'y', labelsize = 20)
    #plt.show()



    ##plot selection function over z with fixed m
    #def plot_P_over_z(m = 5e14, q = 8.5):
        #zlist = np.arange(0.01,1.1,0.1)
        #sel = selection_function()
        #ylist = test.Y500(m,zlist)
        #tlist = test.theta500(m,zlist)
        #Plist = np.array([sel.P_scatter(ylist[i],tlist[i],q) for i in range(len(zlist))])
        #plt.plot(zlist, Plist)
    
    #mlist = np.arange(3e14,17e14,2e14)
    #for m in mlist:
        #plot_P_over_z(m)
    #plt.xlabel('redshift')
    #plt.ylabel(r'$P(q|qm(Y,\theta))$')
    #plt.xlim(0,1)
    #plt.title('selection funciton of observing fixed M at different redshift, qm = 8.5')
    #plt.legend(['M = ' + str(m) + 'Ms' for m in mlist],loc = 'center right')
    #plt.show()
        
    #m = 5e14
    #zlist = np.arange(0.1,1.0,0.05)
    #ylist = test.Y500(m,zlist)
    #tlist = test.theta500(m,zlist)
    #sel = selection_function()
    #Plist = np.array([sel.P_scatter(ylist[i],tlist[i],6) for i in range(len(zlist))])
    #plt.plot(zlist,ylist*1000, zlist, tlist, zlist, 10*Plist)
    #plt.show()
    #comovinglist = np.array([cd.diff_comoving_volume(z, **test.cosmo)/(1.0+z)**3 for z in zlist])
    #print comovinglist




    #plot differential completeness
    sel = selection_function()
    tlist = [1.0, 6.0, 17.00, 31.0]
    ylist = np.arange(-4,-1.3,0.01)
    clist = [np.array([sel.completeness(10**y,theta,8) for y in ylist]) for theta in tlist]
    plt.figure(figsize = (12,8))
    plt.plot(ylist,clist[0], ylist,clist[1],ylist,clist[2],ylist,clist[3], linewidth = 2.0)
    plt.xlabel(r'$\log(Y_{500})$',fontsize = 40)
    plt.ylabel(r'$\chi(Y,\theta,q)$', fontsize = 40)
    plt.tick_params(axis='x', labelsize = 20)
    plt.tick_params(axis='y', labelsize = 20)
    plt.legend([r'$\theta$ = ' + str(t) + 'arcmin' for t in tlist],loc = 'upper left',fontsize = 20)
    plt.title(r'$q_{cut}$ = 8', fontsize = 50)
    plt.show()
    
    
    
    ##plot the cluster count above a cutoff q with respect to z (equivalent to Fig,4 in reading 1.1)
    #qlist = np.arange(6,10.1,1)
    #predict_count = np.array([[test.dNdzdq(z,q)*0.1 for q in qlist] for z in np.arange(0.05,1.01,0.1)])
    #from SZdata_to_bins import count
    #zlist = np.arange(0.05,1.01,0.1)
    #cutoff = 0
    #pre_z = np.array([sum(row) for row in predict_count[:,cutoff:]])
    #exp_z = np.array([sum(row) for row in count[:,cutoff:]])
    
    #flatzlist = np.repeat(zlist - 0.05,2)
    #flatzlist = np.delete(flatzlist,1)
    #flatzlist = np.append(flatzlist, zlist[-1] + 0.05)
    #flatpre = np.repeat(pre_z,2)
    
    #pre, = plt.plot(flatzlist, flatpre)
    
    #plt.errorbar(zlist,exp_z,yerr = np.sqrt(exp_z),linestyle="None",color='red')
    #exp, = plt.plot(zlist,exp_z, 'ro')
    #plt.legend([pre,exp],['prediction','planck result'],loc = 'upper right')
    #plt.xlim(0,1)
    #plt.xlabel('redshift')
    #plt.ylabel('cluster counts')
    #plt.title('q cutoff = ' + str(qlist[cutoff]-0.5))
    #plt.show()
    
    
    ##plot the conversion ratio of M500 and Mt as a function of redshift
    #zlist = np.arange(0.0,1,0.1)
    #rlist = np.array([test.MttoM500(1,z) for z in zlist])
    #plt.plot(zlist, rlist)
    #plt.xlabel('redshift')
    #plt.ylabel(r'$M_{500}/M_t$')
    #plt.show()
    
    

    ##calculate the difference of mass conversion when using different c in the conversion formula
    #zlist = np.arange(0.0,1.1,0.1)
    #def plot_conversion_c(c = 4.):
        #test.c = c
        #rlist = np.array([test.MttoM500(1,z) for z in zlist])
        #a = plt.plot(zlist, rlist)
        ##plt.legend(a, c)
    #clist = np.linspace(3,5,3)
    #for i in clist:
        #plot_conversion_c(i)
    #plt.legend(['c = ' + str(c) for c in clist], loc = 'lower right')
    #plt.xlabel('redshift', fontsize = 18)
    #plt.ylabel(r'$M_{500}/M_t$', fontsize = 18)
    #plt.title(r'$M_{500}/M_t$ with different c',fontsize = 20)
    #plt.tick_params(axis = 'x', labelsize = 20)
    #plt.tick_params(axis = 'y', labelsize = 20)
    #plt.show()


    

    






