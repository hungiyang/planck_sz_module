#Show plots inline, and load main getdist plot module and samples class
import numpy as np
#from __future__ import print_function
from getdist import plots, MCSamples


def plot_posterior(path,label = 'haha'):
    with open(path) as f:
        raw1 = np.array([[float(x) for x in line.split()] for line in f if line[0] != '#'])
    data1 = raw1[13000:-1:7,[1,5,0,6]]
    names = [r'\Omega_M',r'\alpha',r'\sigma_8',r'b']
    labels =  [r'\Omega_M',r'\alpha',r'\sigma_8',r'b']
    samples1 = MCSamples(samples=data1,names = names, labels = labels)
    g = plots.getSubplotPlotter()
    g.triangle_plot([samples1], filled=True
               ,legend_labels=[label])
    return g

if __name__ == '__main__':
    g = plot_posterior('MCMC/bias_float_alpha_NGC_dNdz6_2.txt')
    g.export('test.pdf')
